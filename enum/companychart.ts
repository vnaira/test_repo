export class CompanyChart {
  public static COMPANY = {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Company'
    },
    exporting: {enabled: false},
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %',
          style: {
            color: ('#333' && '#222') || 'black'
          }
        }
      }
    },
    series: [{
      name: 'Brands',
      colorByPoint: true,
      data: [{
        name: 'Microsoft',
        y: 40
      }, {
        name: 'IBM',
        color: '#3c5a99',
        y: 25,
        sliced: true,
        selected: true
      }, {
        name: 'Oracle Corporation',
        y: 20
      }, {
        name: '	Adobe',
        y: 25
      }]
    }]
  }
}
