import {FormGroup } from '@angular/forms';
import { Messages } from '../../assets/resources/messages'

export class ErrorHandler {
	// Handler for audience step
	public static audience(audienceForm: FormGroup, callback: Function) {
		let countries = audienceForm.controls['countries'];
		if (countries.invalid && countries.errors['required']) {
			return callback(Messages.COUNTRY_NOT_SELECTED);
		}
		callback();
	}

	// Handler for objective step
	public static objective(objective, callback: Function) {
		if (!objective) {
			return callback(Messages.PLS_SELECT_OBJECTIVE);
		}
		callback();
	}

	// Handler for schedule step
	public static schedule(budgetType, scheduleForm: FormGroup, callback: Function) {
		let startDate = scheduleForm.controls['startDate'];
		let endDate = scheduleForm.controls['endDate'];
		let bidAmount = scheduleForm.controls['bidAmount'];
		let budget = scheduleForm.controls[budgetType];

		if (!budgetType || (budget.invalid && budget.errors['required'])) {
			return callback(Messages.INPUT_BUDGET_MISSING);
		}

		if (startDate.invalid && startDate.errors['required']) {
			return callback(Messages.START_DATE_NOT_SELECTED);
		}

		if (endDate.invalid && endDate.errors['required']) {
			return callback(Messages.END_DATE_NOT_SELECTED);
		}

		if (bidAmount.invalid && bidAmount.errors['required']) {
			return callback(Messages.BID_MISSING);
		}

		callback();
	}
}

