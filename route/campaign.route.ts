import { Routes } from '@angular/router';

import { CreateComponent } from '../component/create/create.component';
import { ListComponent } from '../component/list/list.component';
import { LeadComponent } from '../component/lead/lead.component';
import { CreateAccountComponent } from '../component/create-account/create-account.component';
import { CreateAudienceComponent } from '../component/create-audience/create-audience.component';
import { CreatePlacementsComponent } from '../component/create-placements/create-placements.component';
import { CreateObjectiveComponent } from '../component/create-objective/create-objective.component';
import { CreateScheduleComponent } from '../component/create-schedule/create-schedule.component';
import { CreateAdFormatComponent } from '../component/create-ad-format/create-ad-format.component';
import { CreateAdPostComponent } from '../component/create-ad-post/create-ad-post.component';



export const CampaignRoutes: Routes = [
{
  path: 'create',
  component: CreateComponent,
  children: [
  {
    path: '',
    component: CreateObjectiveComponent
  },
  {
    path: 'account',
    component: CreateAccountComponent
  },
  {
    path: 'audience',
    component: CreateAudienceComponent
  },
  {
    path: 'placements',
    component: CreatePlacementsComponent
  },
  {
    path: 'objective',
    component: CreateObjectiveComponent
  },
  {
    path: 'schedule',
    component: CreateScheduleComponent
  },
  {
    path: 'ad-format',
    component: CreateAdFormatComponent
  },
  {
    path: 'ad-post',
    component: CreateAdPostComponent
  }
  ]
},
{
  path: 'list',
  component: ListComponent
},
{
  path: 'lead',
  component: LeadComponent
}, {
    path: '',
    component: ListComponent
  },
];
