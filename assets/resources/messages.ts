export class Messages {

  public static PLS_WAIT = "Please Wait..";

  public static LOADING_PAGELIST = "Loading user's page list";

  public static CNT_LOAD_PAGELIST = "Can not load page list";

  public static PLS_INSERT_CMP_NAME = "Please insert campaign name";

  public static SAVING_CMP = "Saving campaign…";

  public static NOT_SAVED = "Campaign not saved";

  public static SAVED = "Saved";

  public static CMP_SCS_SAVED = "Campaign successfully saved";

  public static PUBLISHING_CMP = "Publishing campaign...";

  public static SAVED_BUT_NOT_PBD = "Campaign saved but not published";

  public static PUBLISHED = "Campaign published";

  public static CMP_SCS_SAVED_PBD = "Campaign saved and successfully published";

  public static LOADING_POSTLIST = "Loading page's post list";

  public static CNT_LOAD_POSTLIST = "Can not load post list";

  public static INVALID_BUDGET = "Invalid budget";

  public static BUDGET_LIMIT = "Budget should be from %min% to %max%";

  public static INVALID_BID = "Invalid bid amount. Please enter a valid one";

  public static BID_LIMIT = "Bid amount should be from %min% to %max%";

  public static DELETING_CMP = "Deleting selected campaigns…";

  public static CNT_DELETE_CMP = "Can not delete campaigns…";

  public static DONE = "Done";

  public static CMP_SCS_DELETED = "Selected campaigns successfully deleted";

  public static LAUNCHING_CMP = "Launching campaign…";

  public static CNT_LAUNCH_CMP = "Can not launch the campaign";

  public static LAUNCHED = "Campaign launched";

  public static SCS_LAUNCHED_CMP = "Campaign successfully launched";

  public static STOPPING_CMP = "Stopping the campaign…";

  public static CNT_STOP_CMP = "Can not stop the campaign";

  public static STOPPED = "Campaign stopped";

  public static SCS_STOPPED_CMP = "Campaign successfully stopped";

  public static CONNECTING = "Connecting…";

  public static TRY_CONNECT = "Trying to connect to the Facebook account…";

  public static CNT_LOGIN_FB = "Can not login to Facebook…";

  public static CNT_CONNECT_FB = "Can not connect Facebook account…";

  public static SAVING_TOKEN = "Saving access token…";

  public static FB_SCS_CONNECTED = "Facebook account successfully connected";

  public static CNT_GET_CMPLIST = "Can not get campaign list";

  public static COUNTRY_NOT_SELECTED = "Country not selected. Please select.";

  public static PLS_SELECT_OBJECTIVE = "Please select an objective";

  public static INPUT_BUDGET_MISSING = "Input budget is missing, please insert";

  public static START_DATE_NOT_SELECTED = "Start date not selected. Please select.";

  public static END_DATE_NOT_SELECTED = "End date not selected. Please select.";

  public static BID_MISSING = "Input bid amount is missing, please insert";

  public static EXCLUDED_ALERT = 'You have already added this page in “Exclude people who like your page” connection type.';

  public static LIKER_ALERT = 'You have already added this page in “People who like your page” connection type.';

}

