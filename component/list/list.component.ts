import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Constants} from '../../enum/constants';

import {DataService} from '../../service/data.service';
import {MessageBoxService} from '../../service/messagebox.service';
import {FBService} from '../../service/facebook.service';
import {UserService} from '../../service/user.service';
declare var jQuery:any;

import { Messages } from '../../../assets/resources/messages'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  deleteList: any[] = [];

  dummyCampaignList: any[];
  campaignList: any[];
  dummyChartData: any;
  dummyAdAccounts: any[];
  filterTypes: any[];
  exportTypes: any[];

  spendTime: any[];
  breakdowns: any[];
  showClusteringDialog: boolean;
  pie1: any;
  pie2: any;
  listLoadingValue = 0;

  constructor(private messageBox: MessageBoxService,
              private dataService: DataService,
              private fbService: FBService,
              private messagebox: MessageBoxService,
              private userService: UserService,
              private router: Router) {

    //TODO: optimize
    setTimeout(() => {
      this.listLoadingValue = 50;
    }, 100);

    this.dataService.getCampaignList((err, list) => {
      //TODO: remove zibil
      setTimeout(() => {
        this.listLoadingValue = 100;
      }, 3000);
      this.listLoadingValue = 99;
      if (err) {
        var message = err.extras ? err.extras.message || err.extras[0] : err.message;
        this.messageBox.error(Messages.CNT_GET_CMPLIST, message, 5000);
        return;
      }

      this.campaignList = this.parseCampaignList(list.data);
    });

    this.filterTypes = Constants.FILTER_TYPES;
    this.exportTypes = [
      {
        label: 'Export to CSV',
        value: 1
      }, {
        label: 'Export to XLS',
        value: 2
      }];

    this.spendTime = [{
      label: '7 days',
      value: 0
    }, {
      label: '30 days',
      value: 5
    },
      {
        label: '365 days',
        value: 1
      },
      {
        label: 'Lifetime',
        value: 2
      }];

    this.breakdowns = [
      {
        label: 'Age',
        value: 5
      },
      {
        label: 'Gender',
        value: 1
      },
      {
        label: 'Targeting',
        value: 2
      },
      {
        label: 'Language',
        value: 3
      },
      {
        label: 'Platforms',
        value: 8
      },
      {
        label: 'Placement',
        value: 8
      }
    ];
  }


  ngOnInit() {
  }

  create() {
    location.href = "#/create";
  }

  getRowStyle(data, index) {
    return (index < 3) ? 'pink-row' : '';
  }

  onRowClick() {
    // this.showClusteringDialog = true;
  }

  onChartClicked() {
    location.href = "#/lead"
  }

  onSelectAll(val) {
    this.campaignList.map((data) => {
      data.selected = val;
      this.onRowCheck(data);
    });
  }

  onDeleteClick() {
    if (!this.deleteList.length) {
      return;
    }
    this.messagebox.preloader(Messages.PLS_WAIT, Messages.DELETING_CMP);
    this.dataService.deleteCampaigns(this.deleteList, (err) => {
      this.messagebox.hidePreloader();
      if (err) {
        console.log(err);
        var message = err.extras ? err.extras.message || err.extras[0] : err.message;
        this.messageBox.error(Messages.CNT_DELETE_CMP, message, 5000);
        return;
      }
      this.messagebox.success(Messages.DONE, Messages.CMP_SCS_DELETED, 5000);
      location.reload();

    });
  }

  onRowCheck(rowData) {
    var index = rowData.selected && this.deleteList.indexOf(rowData.campaignId);

    if (rowData.selected && index === -1) {
      this.deleteList.push(rowData.campaignId);
      return;
    }

    if (!rowData.selected && index !== -1) {
      this.deleteList.splice(index, 1);
      return;
    }
  }

  onStatusChange(data) {
    if (!data) {
      return;
    }

    if (data.isActive && data.campaignId) {
      this.messagebox.preloader(Messages.PLS_WAIT, Messages.LAUNCHING_CMP);
      this.fbService.launchCampaign(data.campaignId, (err, res) => {
        this.messagebox.hidePreloader();
        if (err) {
          console.log(err);
          var message = err.extras ? err.extras.message || err.extras[0] : err.message;
          this.messageBox.error(Messages.CNT_LAUNCH_CMP, message, 5000);
          return;
        }
        this.messagebox.success(Messages.LAUNCHED, Messages.SCS_LAUNCHED_CMP, 3000);
      });
      return;
    }

    if (!data.isActive && data.campaignId) {
      this.messagebox.preloader(Messages.PLS_WAIT, Messages.STOPPING_CMP);
      this.fbService.pauseCampaign(data.campaignId, (err, res) => {
        this.messagebox.hidePreloader();
        if (err) {
          console.log(err);
          var message = err.extras ? err.extras.message || err.extras[0] : err.message;
          this.messageBox.error(Messages.CNT_STOP_CMP, message, 5000);
          return;
        }
        this.messagebox.success(Messages.STOPPED, Messages.SCS_STOPPED_CMP, 3000);
      });
      return;
    }
  }


  // private methods

  private parseCampaignList(list: any[]) {

    let result = [];
    if (list.length === 0) {
      return result;
    }

    list.map((item) => {
      result.push({
        campaignId: item.campaignId._id,
        campaign: item.campaignId.name,
        budget: item.dailyBudget || item.lifetimeBudget,
        amount: item.bidAmount,
        starts: item.startTime,
        ends: item.endTime,
        isActive: item.campaignId.status === 'ACTIVE',
        published: item.fbId
      });
    });

    return result;
  }

  private connectToFb() {
    this.messagebox.preloader(Messages.CONNECTING, Messages.TRY_CONNECT);
    this.fbService.login((err, token) => {
      if (err || !token) {
        return this.messagebox.error('', Messages.CNT_CONNECT_FB, 5000);
      }

      this.messagebox.preloader(Messages.CONNECTING, Messages.SAVING_TOKEN);
      this.userService.saveFacebookToken(token, (err) => {
        this.messagebox.hidePreloader();
        if (err) {
          this.messagebox.error('', Messages.CNT_LOGIN_FB, 5000);
          return;
        }
        this.messagebox.success(Messages.CONNECTING, Messages.FB_SCS_CONNECTED, 5000);
        location.href = '#/create';
      })
    });
  }

}
