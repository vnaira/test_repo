import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {SelectItem} from 'primeng/primeng';

import { CreateMenuService } from '../../service/create-menu.service';
import { FBService } from '../../service/facebook.service';
import { Constants } from '../../enum/constants'
import { DummyData } from '../../enum/dummyData'
import { Gauges } from '../../enum/gauges'

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  menuData: any;
  adAccounts: SelectItem[];
  gauge1: Object;
  gauge2: Object;
  gauge1Instance: any;
  gauge2Instance: any;

  reachTooltip: any;
  costTooltip: any;

  get guageHidden(): boolean {
    var url = String(this.router.routerState.snapshot.url);
    switch (url) {
      case '/create/schedule':
      case '/create/audience':
      case '/create/placements':
      return false;
    }
    return true;
  }


  constructor(
    private createMenu: CreateMenuService,
    private fbService: FBService,
    private router: Router
    ) {
    this.createMenu.reset();
    this.createMenu.audienceForm.controls['ageMin'].setValue('');
    this.createMenu.audienceForm.controls['ageMax'].setValue('');

    this.menuData = createMenu.menuData;
    this.gauge1 = Gauges.GAUGE_1;
    this.gauge2 = Gauges.GAUGE_2;


    fbService.accounts((err, accounts) => {
      if (err || !accounts) {
        console.log(err);
        return;
      }

      this.adAccounts = [];
      // TODO: optimize
      this.menuData.campaignData.adAccount = accounts[0];
      accounts.map((item) => {
        this.adAccounts.push({
          label: item.name,
          value: item
        });
      });
    });
  }

  ngOnInit() {
  }

  subMenuClick(index, item ) {
    this.createMenu.subMenuClick(index, item);
  }

  menuClick(index, item) {
    this.createMenu.menuClick(index, item);
  }

  showTooltip(tooltipType) {
    this.menuData[tooltipType] = true;
  }

  hideTooltip(tooltipType) {
    this.menuData[tooltipType] = false;
  }

  initGauge1(instance){
    var self = this;
    self.gauge1Instance = instance;
    var point = self.gauge1Instance['series'][0].points[0];

    setInterval(function() {
      point.update(self.menuData.potentialReach || 0);
    }, 1000);
  }


  initGauge2(instance){
    var self = this;
    self.gauge2Instance = instance;
    var point = self.gauge2Instance['series'][0].points[0];

    setInterval(function() {
      point.update(self.menuData.suggestedCost || 0);
    }, 1000);
  }
}
